//
//  NEShakeGestureManager.h
//  NetworkEye
//
//  Created by coderyi on 15/11/5.
//  Copyright © 2015年 coderyi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NEShakeGestureManager : NSObject
- (void)install;

- (void)uninstall;
@end
